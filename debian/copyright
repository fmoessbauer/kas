Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Jan Kiszka <jan.kiszka@siemens.com>
Upstream-Name: kas
Source: https://github.com/siemens/kas

Files: *
Copyright: 2017-2024, Siemens AG
License: Expat

Files: kas/plugins/checkout.py
       kas/plugins/for_all_repos.py
       tests/test_commands.py
Copyright: Konsulko Group, 2020
License: Expat

Files: contrib/oe-git-proxy
Copyright: 2013, Intel Corporation.
License: GPL-2

Files: kas/plugins/menu.py
Comment: Parts of this were based on kconfiglib, examples/menuconfig_example.py
Copyright: Siemens AG, 2021
           2011-2019, Ulf Magnusson <ulfalizer@gmail.com>
License: Expat and ISC

Files: debian/*
Copyright: 2020 Linutonix GmbH <info@linutronix.de>
           2021-2022 Bastian Germann
           2024 Felix Moessbauer
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

License: GPL-2
 A copy of the GPL version 2 is located in /usr/share/common-licenses/GPL-2
 on Debian systems.
